class ShootsController < ApplicationController
  before_filter :fetch_spot
  before_filter :fetch_shoot, only: :show

  def show
  end

  private

  def fetch_spot
    @spot = Spot.find params[:spot_id]
  end

  def fetch_shoot
    @shoot = Shoot.find params[:id]
  end
end
