class UsersController < ApplicationController
  load_and_authorize_resource

  before_filter :authenticate_user!, only: [:show, :edit, :update]
  before_filter :fetch_user, only: [:show, :edit, :update]

  def show
  end

  private

  def fetch_user
    @user = current_user
  end
end
