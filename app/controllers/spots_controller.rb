class SpotsController < ApplicationController
  before_filter :fetch_spot, only: :show

  respond_to :html, except: :markers
  respond_to :json, only: :markers

  def index
    @spots = Spot.all
  end

  def markers
    respond_with Spot.all.to_json
  end

  private

  def fetch_spot
    @spot = Spot.find params[:id]
  end
end
