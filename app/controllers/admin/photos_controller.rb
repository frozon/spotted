class Admin::PhotosController < Admin::BaseController
  before_filter :load_spot
  before_filter :load_shoot

  def new
    @photo = Photo.new
  end

  def create
    @photo = Photo.new photo_params
    @photo.shoot = @shoot
    if @photo.save
      redirect_to [:new, :admin, @spot, @shoot, :photo]
    else
      render action: 'new'
    end
  end

  private

  def load_spot
    @spot = Spot.find params[:spot_id]
  end

  def load_shoot
    @shoot = Shoot.find params[:shoot_id]
  end

  def photo_params
    params.require(:photo).permit(:file)
  end

end
