class Admin::ShootsController < Admin::BaseController
  before_filter :fetch_spot
  before_filter :fetch_shoot, only: [:show, :edit, :update, :delete]

  layout 'application'

  def index
    @shoots = Shoot.all
  end

  def new
    @shoot = Shoot.new
  end

  def create
    @shoot = Shoot.new shoot_params
    @shoot.spot = @spot
    if @shoot.save
      redirect_to [:admin, @spot, @shoot]
    else
      render action: 'new'
    end
  end

  private

  def fetch_spot
    @spot = Spot.find params[:spot_id]
  end

  def fetch_shoot
    @shoot = Shoot.find params[:id]
  end

  def shoot_params
    params.require(:shoot).permit(:shooted_at)
  end
end
