class Admin::UsersController < Admin::BaseController
  before_filter :fetch_user, only: [:show, :edit, :update, :delete]

  layout 'application'

  def index
    @users = User.all
  end

  def update
    if @user.update_attributes user_params
      redirect_to [:admin, @user]
    else
      render action: 'edit'
    end
  end

  private

  def fetch_user
    @user = User.find params[:id]
  end

  def user_params
    params[:user].delete_if { |k, v| v.empty? }
    params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :admin)
  end
end
