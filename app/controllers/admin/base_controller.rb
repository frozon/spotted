class Admin::BaseController < ApplicationController
  before_filter :admin_restricted

  def admin_restricted
    unless can? :access, :admin
      redirect_to root_path
    end
  end
end