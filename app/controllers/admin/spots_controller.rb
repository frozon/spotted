class Admin::SpotsController < Admin::BaseController
  before_filter :fetch_spot, only: [:show, :edit, :update, :delete]

  layout 'application'

  def index
    @spots = Spot.all
  end

  def new
    @spot = Spot.new
  end

  def create
    @spot = Spot.new spot_params
    if @spot.save
      redirect_to [:admin, @spot]
    else
      render action: 'new'
    end
  end

  private

  def fetch_spot
    @spot = Spot.find params[:id]
  end

  def spot_params
    params.require(:spot).permit(:name, :country)
  end
end
