class School < ActiveRecord::Base
  has_many :shoots
  has_many :photos, through: :shoots
  has_many :orders

  validates :name, presence: true, uniqueness: true
  validates :coupon, uniqueness: true
end
