class Shoot < ActiveRecord::Base
  belongs_to :spot
  has_many :photos

  validates :spot, presence: true
  validates :shooted_at, presence: true

end
