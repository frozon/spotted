class Ability
  include CanCan::Ability

  def initialize(user)
    @user = user

    load @user.admin? ? :admin : :default
  end

  def load *permissions
    permissions.map {|perm| perm.to_s.gsub '::', '_' }.each {|perm| self.send perm if self.respond_to? perm }
  end

  def default
    can :manage, User, id: @user.id
  end

  def admin
    can :access, :admin
    can :manage, :all
  end
end
