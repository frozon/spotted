class Order < ActiveRecord::Base
  belongs_to :user
  has_many :payments
  has_many :lines
  has_many :photos, through: :lines

  validates :user, presence: true

  def paid?
    payments.where('payed_at is not null').empty? ? false : true
  end

end
