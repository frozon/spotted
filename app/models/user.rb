class User < ActiveRecord::Base
  devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable, :confirmable

  has_many :orders
  has_many :payments, through: :orders
  has_many :lines, through: :orders
  has_many :photos, through: :lines

  validates :first_name, presence: true

  def name
    [first_name, last_name].join(' ')
  end
end
