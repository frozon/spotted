class Line < ActiveRecord::Base
  belongs_to :order
  belongs_to :photo

  validates :order, presence: true
  validates :photo, presence: true
  validates :quantity, presence: true
end
