class Spot < ActiveRecord::Base
  has_many :shoots
  has_many :photos, through: :shoots

  validates :name, presence: true, uniqueness: { scope: :country }
  validates :country, presence: true

  geocoded_by :address, latitude: :lat, longitude: :lng

  before_save :geocode

  def geocode!
    geocode
    save
  end

  def address
    [name, country].join(', ')
  end

  def as_json options={}
    {name: name, lat: lat, lng: lng, id: id}
  end

end
