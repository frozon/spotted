class Photo < ActiveRecord::Base
  belongs_to :shoot

  mount_uploader :file, FileUploader

  validates :shoot, presence: true
  validates :file, presence: true

end
