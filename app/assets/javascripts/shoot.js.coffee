class Shoot
  constructor: (element) ->
    @el = $(element)
    @overlay = @el.find '.overlay'

    @el.bind 'mouseenter', (e) =>
      @overlay.show()

    @el.bind 'mouseleave', (e) =>
      @overlay.hide()

$('.shoot').each ->
  new Shoot this