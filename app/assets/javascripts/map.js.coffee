class Map
  constructor: (element) ->
    @element = element
    zoom = if $(@element).attr('data-zoom') == undefined
      10
    else
      parseInt($(@element).attr('data-zoom'))

    center = if $(@element).attr('data-center-lat') == undefined
      new google.maps.LatLng(-20.257, 57.497)
    else
      new google.maps.LatLng(parseFloat($(@element).attr('data-center-lat')), parseFloat($(@element).attr('data-center-lng')))

    styles = [
      {
        featureType: "administrative",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"poi",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"road",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"transit",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"landscape.man_made",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"landscape.natural.terrain",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"water",
        elementType:"labels",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"water",
        stylers:[
          {"visibility":"on"},
          {"color":"#f5f5f5"}
        ]
      },{
        featureType:"landscape",
        stylers:[{"color":"#526f8d"}]
      },{
        featureType:"water",
        elementType:"labels.text",
        stylers:[{"visibility":"off"}]
      },{
        featureType:"landscape",
        elementType:"labels",
        stylers:[{"visibility":"off"}]
      }
    ]

    styledMap = new google.maps.StyledMapType(styles, {name: "Styled Map"});

    @options = {
      scrollwheel:        false
      zoom:               zoom
      center:             center
      mapTypeControl:     false
      panControl:         false
      streetViewControl:  false
    }
    @map = new google.maps.Map @element, @options
    @map.mapTypes.set 'map_style', styledMap
    @map.setMapTypeId 'map_style'
    @markers = new Array

    if $(@element).attr('data-marker-name') isnt undefined
      new google.maps.Marker {
        position:   new google.maps.LatLng(parseFloat($(@element).attr('data-marker-lat')), parseFloat($(@element).attr('data-marker-lng')))
        title:      $(@element).attr('data-marker-name')
        map:        @map
        draggable:  true
        icon:       '/assets/marker_dot.png'
      }

    @layer = {
      source: $(@element).attr 'data-source'
    }
    @updateLayer()

  updateLayer: ->
    $.getJSON @layer.source, (data) =>
      @render data

  render: (data) ->
    for spot in data
      console.log spot
      point = new google.maps.LatLng spot.lat, spot.lng
      marker = new google.maps.Marker {
        position:   point
        title:      spot.name
        map:        @map
        draggable:  true
        url:        '/spots/'+spot.id
        icon:       '/assets/marker_dot.png'
      }

      @markers.push marker

      google.maps.event.addListener marker, 'click', (e) ->
        window.location.href = marker.url


$('.map').each ->
  console.log '[ ] Loading map'
  new Map this