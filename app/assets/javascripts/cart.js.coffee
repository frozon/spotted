# Cart class
# Usage:
#   cart = new Cart
#   item = {id: 1, name: 'Photo 12', size: 12}
#   cart.add item, 10
#   cart.remove item, 1
#   cart.empty

class Cart
  constructor: ->
    @widget = $('#cart')
    @items = []
    @reload()
    @updateWidget()

  add: (new_item, qte = 1) ->
    item = null
    for i in @items
      if i.id is new_item.id
        console.log '[ ] Same item found, ' + qte + ' to the quantity'
        item = i
        if i.qte isnt undefined
          i.qte += qte
        else
          i.qte = 1 + qte
    if item is null
      console.log '[ ] Did not find matching item adding a new one'
      new_item.qte = qte
      @items.push new_item
    console.log @items
    @update()

  remove: (item, qte = 1) ->
    console.log @items
    for i in @items
      console.log i
      if i.id is item.id
        if qte < i.qte
          i.qte -= qte
        else
          i.qte = 0
    @update()
    console.log @items

  empty: ->
    @items = []
    @update()

  reload: ->
    @storage = localStorage.getItem 'spotted.cart'
    if @storage is null
      console.log '[!] Spotted cart does not exists, creating it'
      @storage = localStorage['spotted.cart'] = '[]'
    else
      console.log '[ ] Spotted cart exists, reloading items'
    @items = JSON.parse(@storage)

  update: ->
    @storage = JSON.stringify @items
    localStorage.setItem 'spotted.cart', JSON.stringify(@storage)
    @updateWidget()

  updateWidget: ->
    qte = 0
    for item in @items
      if item.qte isnt undefined
        qte += item.qte
      else
        qte += 1
    @widget.find('#item-count').html 'There is ' + qte + ' in your cart'

# if typeof(localStorage) is "undefined"
#   console.log '[!] LocalStorage unsupported'
# else
#   console.log '[ ] LocalStorage suppoted'
#   cart = new Cart

#   cart.empty()
#   item = {id: 1, name: 'Photo 1', size: 12}
#   console.log item

#   item1 = {id: 2, name: 'Photo 2', size: 12}
#   console.log item1

#   console.log '• adding item'
#   cart.add item
#   console.log '• adding this item a second time but 1 times'
#   cart.add item1, 20
#   console.log '• removing item'
#   cart.remove item1
#   console.log item
#   console.log '• removing item with qte 2'
#   cart.remove item, 1