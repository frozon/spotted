class Share
  constructor: (element) ->
    @element = $(element)
    @element.bind 'click', (e) =>
      e.preventDefault()
      @fbShare()

  fbShare: ->
    window.open "http://www.facebook.com/sharer.php?u=#{@element.attr('data-url')}"

$('.share').each ->
  new Share this