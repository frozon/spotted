# encoding: utf-8

class FileUploader < CarrierWave::Uploader::Base

  # Include RMagick or MiniMagick support:
  # include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  # Choose what kind of storage to use for this uploader:
  storage :file
  # storage :fog

  # Override the directory where uploaded files will be stored.
  # This is a sensible default for uploaders that are meant to be mounted:
  def store_dir
    "uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

  # Provide a default URL as a default if there hasn't been a file uploaded:
  # def default_url
  #   # For Rails 3.1+ asset pipeline compatibility:
  #   # ActionController::Base.helpers.asset_path("fallback/" + [version_name, "default.png"].compact.join('_'))
  #
  #   "/images/fallback/" + [version_name, "default.png"].compact.join('_')
  # end

  # Process files as they are uploaded:
  # process :scale => [200, 300]
  #
  # def scale(width, height)
  #   # do something
  # end

  # Create different versions of your uploaded files:
  version :thumb do
    process resize_to_fill: [50, 50]
    process :watermark
  end

  version :small do
    process resize_to_fill: [250, 250]
    process :watermark
  end

  version :medium do
    process resize_to_fill: [350, 350]
    process :watermark
  end

  version :big do
    process scale: [800, 600]
    process :watermark
  end

  def watermark
    manipulate! do |img|
      img.combine_options do |cmd|
        cmd.gravity 'Center'
        cmd.fill("lightgray")
        cmd.draw "rotate 325 text 0,0 'SPOTTED'"
        cmd.font "#{Rails.root}/app/assets/fonts/oswald-bold-webfont.ttf"
        cmd.pointsize '50'
      end
      result = img
    end
  end

  def extension_white_list
    %w(jpg jpeg gif png)
  end

  # Override the filename of the uploaded files:
  # Avoid using model.id or version_name here, see uploader/store.rb for details.
  def filename
    "#{mounted_as}_#{model.id}.#{original_filename.gsub(/.*\./, '')}"
  end

end
