require 'test_helper'

class LineTest < ActiveSupport::TestCase
  test "should not create empty line" do
    line = Line.new
    assert !line.save, line.errors.full_messages.inspect
  end

  test "should not create line without photo" do
    user = User.create first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    order = Order.create user: user
    line = Line.new quantity: 1, order: order
    assert !line.save, line.errors.full_messages.inspect
  end

  test "should not create line without quantity" do
    # creating valid order
    user = User.create first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    order = Order.create user: user
    # creating valid photo
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.create spot: spot, shooted_at: Date.today
    photo = Photo.create shoot: shoot, file: File.open(Rails.root.join("test/files/test.jpg"))
    line = Line.new photo: photo, order: order
    assert !line.save, line.errors.full_messages.inspect
  end

  test "should create line with everything" do
    # creating valid order
    user = User.create first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    order = Order.create user: user
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.create spot: spot, shooted_at: Date.today
    photo = Photo.create shoot: shoot, file: File.open(Rails.root.join("test/files/test.jpg"))
    line = Line.new photo: photo, quantity: 1, order: order
    assert line.save
  end
end
