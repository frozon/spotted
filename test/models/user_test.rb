require 'test_helper'

class UserTest < ActiveSupport::TestCase
  test "should not create empty user" do
    user = User.new
    assert !user.save
  end

  test "should not create user without first_name" do
    user = User.new last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    assert !user.save
  end

  test "should not create user without password" do
    user = User.new first_name: 'foo', last_name: 'bar', email: 'foo@bar.com'
    assert !user.save
  end

  test "should not create user with a password length < 8" do
    user = User.new first_name: 'foo', last_name: 'bar', password: 'bar', password_confirmation: 'bar', email: 'foo@bar.com'
    assert !user.save
  end

  test "should not create user with non matching confirmation password" do
    user = User.new first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar', email: 'foo@bar.com'
    assert !user.save
  end

  test "should not create user without email" do
    user = User.new first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar'
    assert !user.save
  end

  test "should not create user with invalid email" do
    user = User.new first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'boo@bar'
    assert !user.save
  end

  test "should create user with right informations" do
    user = User.new first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    assert user.save
  end

  test "user should have 2 orders" do
    assert User.find(1).orders.count == 2
  end

  test "user should have one photo" do
    assert User.find(1).photos.count == 1
  end

  test "user should respond to name" do
    assert User.find(1).name == 'f00 bar'
  end

end
