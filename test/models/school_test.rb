require 'test_helper'

class SchoolTest < ActiveSupport::TestCase
  test "should not create empty school" do
    school = School.new
    assert !school.save
  end

  test "should not create school with duplicated name" do
    School.create name: 'NKS'
    school = School.new name: 'NKS'
    assert !school.save
  end

  test "should not create school with duplicated coupon" do
    School.create name: 'NKS', coupon: 'AZERTY'
    school = School.new name: 'Son Of Kite', coupon: 'AZERTY'
    assert !school.save
  end
end
