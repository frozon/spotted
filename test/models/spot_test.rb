require 'test_helper'

class SpotTest < ActiveSupport::TestCase
  test "should not create spot without name" do
    spot = Spot.new country: "Mauritius"
    assert !spot.save
  end

  test "should not create spot without country" do
    spot = Spot.new name: "Le morne"
    assert !spot.save
  end

  test "should not create spot with duplicated name in the same country" do
    Spot.create name: "Le morne", country: "Mauritius"
    spot = Spot.new name: "Le morne", country: "Mauritius"
    assert !spot.save
  end

  test "should create spot with duplicated name but in different country" do
    Spot.create name: "Le morne", country: "Mauritius"
    spot = Spot.new name: "Le morne", country: "Rodrigues"
    assert spot.save
  end

end
