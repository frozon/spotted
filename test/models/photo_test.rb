require 'test_helper'

class PhotoTest < ActiveSupport::TestCase
  test "should not create empty photo" do
    photo = Photo.new
    assert !photo.save
  end

  test "should not create photo without file" do
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.create spot: spot, shooted_at: Date.today
    photo = Photo.new shoot: shoot
    assert !photo.save
  end

  test "should not create photo without shoot" do
    photo = Photo.new file: File.open(Rails.root.join("test/files/test.jpg"))
    assert !photo.save, photo.errors.full_messages.inspect
  end

  test "should create photo with all informations" do
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.create spot: spot, shooted_at: Date.today
    photo = Photo.new shoot: shoot, file: File.open(Rails.root.join("test/files/test.jpg"))
    assert photo.save, photo.errors.full_messages.inspect
  end
end
