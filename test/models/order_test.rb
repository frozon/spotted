require 'test_helper'

class OrderTest < ActiveSupport::TestCase
  test "should not create empty order" do
    order = Order.new
    assert !order.save
  end

  test "should create order with user" do
    user = User.create first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    order = Order.new user: user
    assert order.save
  end

  test "should have a payment" do
    assert !Order.find(1).payments.empty?
  end

  test "should be unpaid" do
    assert !Order.find(1).paid?
  end

  test "should be paid" do
    assert Order.find(2).paid?
  end
end
