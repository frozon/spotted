require 'test_helper'

class PaymentTest < ActiveSupport::TestCase
  test "should not create empty payment" do
    payment = Payment.new
    assert !payment.save
  end

  test "should create payment" do
    user = User.create first_name: 'foo', last_name: 'bar', password: 'bar2bar0bar', password_confirmation: 'bar2bar0bar', email: 'foo@bar.com'
    order = Order.new user: user
    payment = Payment.new order: order
    assert payment.save
  end
end
