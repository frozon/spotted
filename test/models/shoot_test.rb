require 'test_helper'

class ShootTest < ActiveSupport::TestCase
  test "should not create empty shoot" do
    shoot = Shoot.new
    assert !shoot.save
  end

  test "should not create shoot without spot" do
    shoot = Shoot.new shooted_at: Date.today
    assert !shoot.save
  end

  test "should not create shoot without shooted_at" do
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.new spot: spot
    assert !shoot.save
  end

  test "should create shoot with all informations" do
    spot = Spot.create name: "Le morne", country: "Mauritius"
    shoot = Shoot.new spot: spot, shooted_at: Date.today
    assert shoot.save
  end
end
