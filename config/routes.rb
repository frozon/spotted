Spotted::Application.routes.draw do
  devise_for :users, path: '', path_names: {
    sign_in: 'login',
    sign_out: 'logout',
    sign_up: 'register'
  }

  root to: 'home#index'

  get 'contact',  controller: :home, action: :contact,  as: :contact
  get 'infos',    controller: :home, action: :infos,    as: :infos

  resources :users
  resource :user, path: :account, as: :account

  namespace :admin do
    root 'users#index'

    resources :users
    resources :spots do
      resources :shoots do
        resources :photos
      end
    end
    resources :schools
    resources :orders
  end

  resources :spots do
    collection do
      get :markers
    end
    resources :shoots do
      resources :photos
    end
  end
end
