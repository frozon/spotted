class CreateLines < ActiveRecord::Migration
  def change
    create_table :lines do |t|
      t.references :order, :photo
      t.integer :quantity
      t.timestamps
    end
  end
end
