class CreateSpots < ActiveRecord::Migration
  def change
    create_table :spots do |t|
      t.string :name, :country
      t.float :lat, :lng
      t.timestamps
    end
  end
end
