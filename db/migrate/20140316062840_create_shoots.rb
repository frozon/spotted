class CreateShoots < ActiveRecord::Migration
  def change
    create_table :shoots do |t|
      t.references :spot, :school
      t.date :shooted_at
      t.timestamps
    end
  end
end
