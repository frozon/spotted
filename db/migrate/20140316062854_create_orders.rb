class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user, :school
      t.timestamps
    end
  end
end
