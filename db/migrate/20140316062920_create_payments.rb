class CreatePayments < ActiveRecord::Migration
  def change
    create_table :payments do |t|
      t.references :order
      t.datetime :payed_at, :failed_at
      t.timestamps
    end
  end
end
